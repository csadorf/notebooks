# README

## About

Notebooks, that I share with the world for demonstration of tool usage and concepts.

## How to view the jupyter notebooks

Either clone this repository and view or convert them with [jupyter](http://jupyter.org/) directly or paste the raw notebook url into [nbviewer](http://nbviewer.jupyter.org).